#ifndef BLDCMotor_H
#define BLDCMotor_H

/******************************************************************************/
/**
 *  Direction structure
 */
typedef enum
{
    CW      = 1,  //clockwise
    CCW     = -1, // counter clockwise
    UNKNOWN = 0   //not yet known or invalid state
} Direction;

/******************************************************************************/
extern long sensor_direction0;
extern long sensor_direction;
extern float voltage_power_supply;
extern float voltage_limit;
extern float voltage_sensor_align;
extern int  pole_pairs0;
extern int  pole_pairs;
extern unsigned long open_loop_timestamp0;
extern unsigned long open_loop_timestamp;
extern float velocity_limit;
/******************************************************************************/
void Motor_init0(void);
void Motor_initFOC0(void);
void loopFOC0(void);
void move0(float new_target0);
void Motor_init(void);
void Motor_initFOC(void);
void loopFOC(void);
void move(float new_target);

void setPhaseVoltage(float Uq, float Ud, float angle_el);
void setPhaseVoltage0(float Uq, float Ud, float angle_el);
//void setPwm(float Ua, float Ub, float Uc);
/******************************************************************************/

#endif

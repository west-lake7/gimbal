

#ifndef MYPROJECT_H
#define MYPROJECT_H

/* Includes ------------------------------------------------------------------*/

#include "stm32f10x_it.h" 
#include "usart.h"
#include "delay.h"
#include "timer.h"
#include "iic.h"
#include "spi2.h"

#include "MagneticSensor.h" 
#include "foc_utils.h" 
#include "FOCMotor.h" 
#include "BLDCmotor.h" 
#include "lowpass_filter.h" 
#include "pid.h"

/******************************************************************************/
#define M1_Enable    GPIO_SetBits(GPIOA,GPIO_Pin_3);          //高电平使能
#define M1_Disable   GPIO_ResetBits(GPIOA,GPIO_Pin_3);        //低电平解除
#define M2_Enable    GPIO_SetBits(GPIOB,GPIO_Pin_1);          //高电平使能
#define M2_Disable   GPIO_ResetBits(GPIOB,GPIO_Pin_1);        //低电平解除


#endif

